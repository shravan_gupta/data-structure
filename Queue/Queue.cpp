/*
* Queue.cpp
*
* Class Description: Implementation of an int sequence with enqueue/dequeue in FIFO order
* Class Invariant: Data collection with the following characteristics:
*                   - Each element is FIFO order
*
* Author: Shravan Gupta
*/
#include "Queue.h"

// Desc:  Constructor
Queue::Queue() : size(0), capacity(INITIAL_SIZE), frontindex(0), backindex(0) {
	arr = new int[INITIAL_SIZE];
} // constructor

//Desc: Destructor
Queue::~Queue() {
	size = 0;
	delete[] arr;
}

// Desc:  Inserts element x at the back (O(1))
void Queue::enqueue(int x) {
	if (size == capacity)
	{
		capacity *= 2;
		int* temparr = new int[capacity];
		unsigned j = frontindex;
		for (unsigned i = 0;i < capacity / 2;i++) {
			temparr[i] = arr[j];
			if (j + 1 == capacity / 2)
				j = -1;
			j++;
		}
		delete[]arr;
		arr = temparr;
		frontindex = 0;
		backindex = size;
		size++;
		arr[backindex] = x;
		backindex = (backindex + 1) % capacity;
	}
	else 
	{
		size++;
		arr[backindex] = x;
		backindex = (backindex + 1) % capacity;
	}
} // enqueue


// Desc:  Removes the frontmost element (O(1))
//  Pre:  Queue not empty
void Queue::dequeue() {
	if (size < (capacity / 4))
	{
		if ((capacity / 2) >= INITIAL_SIZE) {
			capacity /= 2;
			int* temparr = new int[capacity];
			unsigned j = 0;
			for (unsigned i = frontindex;i <= backindex;i++) {
				temparr[j] = arr[i];
				j++;
			}
			delete[]arr;
			arr = temparr;
			frontindex = 0;
			backindex = size - 1;
			size--;
			frontindex = (frontindex + 1) % capacity;
		}
		else {
			capacity = INITIAL_SIZE;
			int* temparr = new int[capacity];
			unsigned j = 0;
			for (unsigned i = frontindex;i <= backindex;i++) {
				temparr[j] = arr[i];
				j++;
			}
			delete[]arr;
			arr = temparr;
			frontindex = 0;
			backindex = size - 1;
			size--;
			frontindex = (frontindex + 1) % capacity;
		}
	}
	else
	{
		size--;
		frontindex = (frontindex + 1) % capacity;
	}
} // dequeue


// Desc:  Returns a copy of the frontmost element (O(1))
//  Pre:  Queue not empty
int Queue::peek() const {
    return arr[frontindex];
} // top


// Desc:  Returns true if and only if queue empty (O(1))
bool Queue::isEmpty() const {
    return size == 0;
} // isempty




