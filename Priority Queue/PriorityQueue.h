/*
 * PriorityQueue.h
 *
 * Description: PriorityQueue Declaration
 * Class Invariant: Enqueue and Dequeue in a queue taking care of the sorting and elementCount 
 *
 * Author: Shravan Gupta
 */

#pragma once

#include <iostream>
#include "Node.h"
#include "EmptyDataCollectionException.h"

using namespace std;

class PriorityQueue {

private:

   Node* head;      
   int elementCount;  

   // Utility method - Locates the node that is before the node that should or does
   //                  contain the anElement.
   Node* getNodeBefore(Node* anElement) const;

   // Utility method - Destroy the whole linked list - same as dequeueAll()
   void clear();

   // Utility method - Copy a linked list
   Node* copyLinkedList(const Node* originalHead);

public:

   // Default Constructor
   PriorityQueue();

   // Copy Constructor
   PriorityQueue(const PriorityQueue& rhsPriorityQueue);

   // Destructor
   ~PriorityQueue();

   // Description: Returns the number of elements in the Priority Queue.
   // Time Efficiency: O(1)
   int getElementCount() const;

   // Description: Returns "true" is this Priority Queue is empty, otherwise "false".
   // Time Efficiency: O(1)
   bool isEmpty() const;

   // Description: Inserts newElement in sort order.
   //              It returns "true" if successful, otherwise "false".
   // Precondition: This Priority Queue is sorted.
   // Postcondition: Once newElement is inserted, this Priority Queue remains sorted.
   // Time Efficiency: O(n)
   bool enqueue(Node* newElement);

   // Description: Removes the element with the "highest" priority.
   //              It returns "true" if successful, otherwise "false".
   // Precondition: This Priority Queue is not empty.
   // Time Efficiency: O(1)
   bool dequeue();

   // Description: Returns the element with the "highest" priority.
   // Precondition: This Priority Queue is not empty.
   // Postcondition: This Priority Queue is unchanged.
   // Exceptions: Throws EmptyDataCollectionException if this Priority Queue is empty.
   // Time Efficiency: O(1)
   Node peek() const; //throw(EmptyDataCollectionException);

   // Utility method - Copy a linked list
   Node* copyLinkedList(Node* originalHead);

   // For Testing Purposes
   // Description: Prints the content of "this".
   void printPriorityQueue( ) const;

   // To deal with the warning regarding template and friend function: 
   // I used the first "Introvert" solution described in 
   // https://stackoverflow.com/questions/4660123/overloading-friend-operator-for-template-class
   // The idea: only declare a particular instantiation of the << operator as a friend
   // by inlining the operator.  
   friend ostream & operator<< (ostream & os, const PriorityQueue& rhs) {
      Node* current = rhs.head;

      os << "elementCount = " << rhs.elementCount;
      
      // Traverse the list
      while (current != NULL){
         os << current->getChr(); // Print data
         os << current->getWeight();
         current = current -> next; // Go to next Node
      }

      return os;
   } // end of operator <<

}; // end PriorityQueue

