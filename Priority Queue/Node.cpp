/*
 * Node.cpp
 *
 * Description: Node class Implementation
 * Class Invariant: Make nodes consisting of characters and their frequency values
 *
 * Author: Shravan Gupta
 */

#include"Node.h"

//CONSTRUCTORS
Node::Node()
{
	this->chr = ' ';
	this->weight = 0;
}
Node::Node(char chr, int weight)
{
	this->chr = chr;
	this->weight = weight;
}

//SETTERS
void Node::setChr(const char &chr)
{
	this->chr = chr;
}
void Node::setWeight(const int &weight)
{
	this->weight = weight;
}

//GETTERS
char Node::getChr() const
{
	return chr;
}
	
int Node::getWeight() const
{
	return weight;
}
// end Node.cpp