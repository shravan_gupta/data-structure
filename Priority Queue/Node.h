/*
 * Node.h
 *
 * Description: Node class Declaration
 * Class Invariant: Make nodes consisting of characters and their frequency values
 *
 * Author: Shravan Gupta
 */

#pragma once

#include <cstdio>  // Needed for NULL
#include<iostream>
using namespace std;

class Node
{
public:
	char chr;
	int weight; 
	Node* next;

	// Constructors
	Node();
	Node( char chr, int weight);

	//SETTERS
	void setChr(const char &chr);
	void setWeight(const int &weight);

	//GETTERS
	char getChr() const;
	int getWeight() const;
}; // end node

