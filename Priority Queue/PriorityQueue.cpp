/*
 * PriorityQueue.cpp
 *
 * Description: PriorityQueue Implementation
 * Class Invariant: Enqueue and Dequeue in a queue taking care of the sorting and elementCount 
 *
 * Author: Shravan Gupta
 */

#include<iostream>
#include"PriorityQueue.h"
#include"Node.h"
using namespace std;

// Default Constructor

PriorityQueue::PriorityQueue() {
   head = NULL;
   elementCount = 0;
   // cout << "PriorityQueue Default constructor" << endl; // For learning purposes!
} // end of default constructor

// Copy Constructor
PriorityQueue::PriorityQueue(const PriorityQueue& rhsPriorityQueue) {
   head = copyLinkedList(rhsPriorityQueue.head);
   elementCount = rhsPriorityQueue.elementCount;
   // cout << "PriorityQueue Copy constructor" << endl; // For learning purposes!
}  // end copy constructor

// Utility method - Copy a linked list
Node* PriorityQueue::copyLinkedList( Node* originalHead) {
    Node* copiedHead = NULL;

   if (originalHead != NULL)
   {
      // Build new linked list from given one
      // copiedHead = new Node(originalHead->);
      if ( copiedHead != NULL )
         copiedHead->next = copyLinkedList(originalHead->next);
      else
         return NULL;
   }  // end if

   return copiedHead;
}  // end copylinked list

// Destructor
PriorityQueue::~PriorityQueue() {
   clear();
   // cout << "PriorityQueue destructor" << endl; // For learning purposes!
}  // end destructor

// Utility method - Destroy the whole linked list - same as dequeueAll()
void PriorityQueue::clear()
{
   while (!isEmpty())
      dequeue();
}  // end clear

// Description: Returns the number of elements in the Priority Queue.
int PriorityQueue::getElementCount() const {
   return elementCount;
}

// Description: Returns "true" is this Priority Queue is empty, otherwise "false".
// Time Efficiency: O(1)
bool PriorityQueue::isEmpty() const {
   return ( elementCount == 0 && head == NULL );
}  // end isEmpty


// Description: Inserts newElement in sort order.
//              It returns "true" if successful, otherwise "false".
// Precondition: This Priority Queue is sorted.
// Postcondition: Once newElement is inserted, this Priority Queue remains sorted.
// Time Efficiency: O(n)
bool PriorityQueue::enqueue(Node* newElement) {

   // cout << "PriorityQueue::enqueue" << endl;

   bool ableToEnqueue = false;

   Node* newNode = new Node();
   //newNode

   if ( newNode != NULL ){
         Node* previous = getNodeBefore(newElement);

      if ( isEmpty() || previous == NULL) { // Add at beginning
         newNode->next = head;
         head = newNode;
      }
      else {  // Add after node before
            Node* afterNode = previous->next;
            newNode->next = afterNode;
            previous->next = newNode;
       } // end if

      elementCount++;    // Increment count of elements
      ableToEnqueue = true;
   }

   return ableToEnqueue;
} // end enqueue

// Utility method - Locates the node that is before the node that should or does
//                  contain the anElement.
Node* PriorityQueue::getNodeBefore(Node* anElement) const {

   Node* current = head;
   Node* previous = NULL;

   // Enqueueing the newElement after the already enqueued elements with smaller or equal priority value
   // i.e., preserving the FIFO characteristic of this PriorityQueue
   while ( (current != NULL) && (current->chr <= anElement->chr) )
   {
      previous = current;
      current = current->next;
   } // end while

   return previous;
} // end getNodeBefore

// Description: Removes the element with the "highest" priority.
//              It returns "true" if successful, otherwise "false".
// Precondition: This Priority Queue is not empty.
// Time Efficiency: O(1)
bool PriorityQueue::dequeue() {
   bool ableToDequeue = false;

   if ( !isEmpty() ) {
      Node* current = head;

      // Delete the first node in the linked list
      head = head->next;

      // Return deleted node to system
      current->next = NULL;
      delete current;
      current = NULL;

      elementCount--;  // Decrement count of elements
      ableToDequeue = true;
   }  // end if

   return ableToDequeue;
}  // end remove


// Description: Returns (a copy of) the element with the "highest" priority.
// Precondition: This Priority Queue is not empty.
// Postcondition: This Priority Queue is unchanged.
// Exceptions: Throws EmptyDataCollectionException if this Priority Queue is empty.
// Time Efficiency: O(1)
Node PriorityQueue::peek() const {//;throw(EmptyDataCollectionException) {

   // Enforce precondition
   if ( !isEmpty() )
   {
      Node* node = head;
      return *node;
   }
  /* else
      throw(EmptyDataCollectionException("peek() called with an empty PriorityQueue."));*/

}  // end peek


// For Testing Purposes - See Labs 3 and 4.
// Description: Prints the content of "this".
void PriorityQueue::printPriorityQueue( ) const {
   Node* current = head;

   // Traverse the list
   while (current != NULL){
      cout  << current->getChr(); // Print data
      cout << current->getWeight();
      current = current -> next; // Go to next Node
   }

}  // end printPriorityQueue

//  End of implementation file.