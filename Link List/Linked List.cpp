// Linked List : Defines the entry point for the console application.
//

#include <iostream>
using namespace std;

class Node
{
private:
	int data;
	Node* link;
public:
	Node();
	Node(const int &data, Node* link);

	int getData()const { return data; }
	Node* getLink()const { return link; }

	void setData(const int &data);
	void setLink(Node* link);
};
Node::Node()
{
	data = 0;
	link = nullptr;
}
Node::Node(const int &data, Node* link)
{
	this->data = data;
	this->link = link;
}
void Node::setData(const int &data)
{
	this->data = data;
}
void Node::setLink(Node* link)
{
	this->link = link;
}
void headInsert(Node* &head, const int &data)
{
	Node* temp = new Node();
	temp->setData(data);
	temp->setLink(head);
	head = temp;
}
void insertAfter(Node* afterme, const int &data)
{
	Node* temp = new Node();
	temp->setData(data);
	temp->setLink(afterme->getLink());
	afterme->setLink(temp);
}
Node* searchList(Node* &head, const int & target)
{
	Node* temp = head;
	while (temp != nullptr)
	{
		if (temp->getData() == target)
			return temp;
		else
			temp = temp->getLink();
	}
	return nullptr;
}
void remove(Node* &head, const int &num)
{
	Node* after = head;
	Node* before = head;
	while (after != nullptr)
	{
		if (after->getData() != num)
		{
			before = after;
			after = after->getLink();
		}
		else if (after == head)
		{
			head = head->getLink();
			before = head;
			delete after;
			after = head;
		}
		else
		{
			before->setLink(after->getLink());
			delete after;
			after = before->getLink();
		}
	}
}
void removeRepeat(Node* &head)
{
	Node*after = head->getLink();
	Node*before = head;
	do
	{
		if (before->getData() == after->getData())
		{
			before->setLink(after->getLink());
			before = after->getLink();
			delete after;
			after=before->getLink();
		}
		else
		{
			before = before->getLink();
			after = after->getLink();
		}
	} while (after != nullptr);
}
void removeTail(Node* &head)
{
	Node* before = head;
	Node* after = head->getLink();
	do
	{
		if (after->getLink() == nullptr)
		{
			before->setLink(nullptr);
			delete after;
		}
		else
		{
			before = before->getLink();
			after = after->getLink();
		}
	}while(after->getLink() != nullptr);
}

//void remove(Node* &head, const int &num)
//{
//	if (head == nullptr)
//		return;
//	else if (head->getData() == num)
//	{
//		Node* temp = head;
//		head = head->getLink();
//		delete temp;
//	}
//	else
//	{
//		Node* n1 = head->getLink();
//		Node* n2 = head;
//		while (n1->getLink() != nullptr)
//		{
//			if (n1->getData() == num)
//			{
//				n2->setLink(n1->getLink());
//				delete n1;
//				n1 = n2->getLink();
//			}		
//		}
//	}
//}
void printList(Node* &head)
{
	if (head == nullptr)
		cout << "Empty List" << endl;
	else
	{
		Node* temp = head;
		while (temp != nullptr)
		{
			cout << temp->getData() << "\t";
			temp = temp->getLink();
		}
		cout << endl;
	}
}

int main()
{
	Node* head = NULL;
	for (int i = 5; i > 0; --i)
	{
		// after code works, switch the order of these two statements and test again.
		headInsert(head, 9);
		headInsert(head, i);
	}
	//for (int i = 3; i > 0; --i)
	//{
	//	headInsert(head, i);
	//	//headInsert(head, i);
	//}
	cout << "1 print: ";
	printList(head);
	cout << "\n2 print: ";
	cout << searchList(head, 3) << endl;
	insertAfter(head, 20);
	cout << "\n3 print: ";
	printList(head);
	remove(head, 9);
	cout << "\n4 print: ";
	printList(head);
	remove(head, 20);
	cout << "\n5 print: ";
	printList(head);
	removeTail(head);
	cout << "\n6 print: ";
	printList(head);
	//cout << "\n7 print: ";
	//removeRepeat(head);
	//printList(head);

	system("pause");
	return 0;
}