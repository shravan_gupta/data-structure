/*
* Stack.h
*
* Class Description: Implementation of an int sequence with push/pop in a LIFO order
* Class Invariant: Data collection with the following characteristics:
*                   - Each element is LIFO order.
*
* Author: Shravan Gupta
*/

class Stack
{
private:

	// Desc:  Nodes for a singly-linked list
	class StackNode
	{
	public:
		int data;
		StackNode * next;
	};

	// Desc:  head = ptr to the first StackNode (NULL if empty)
	//        tail = ptr to the last StackNode (NULL if empty)
	//  Inv:  Follows the A2 Spec, namely that the implementation 
	//        is a singly-linked list, with insert/remove 
	//        operations at the list's tail.
	StackNode * head;
	StackNode * tail;

public:

	// Desc:  Constructor
	// Post:  Stack is empty.
	Stack();

	// Desc:  Recycles dynamic pointers.
	// Post:  Stack is deleted.
	~Stack();

	// Desc:  Insert element x to the top of the stack.
	// Post:  One element is added in the list.
	void push(int x);

	// Desc:  Remove and return element at the top of the stack.
	//  Pre:  List must not be empty.
	// Post:  One less element in the list.
	int pop();

	// Desc:  Return the topmost element of the stack.
	//  Pre:  List must not be empty
	// Post:  Top integer is returned 
	int peek() const;

	// Desc:  Checks if the list is empty or not
	bool isEmpty() const;
};


