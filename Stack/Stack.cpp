/*
* Stack.cpp
*
* Class Description: Implementation of an int sequence with push/pop in a LIFO order
* Class Invariant: Data collection with the following characteristics:
*                   - Each element is LIFO order
*                  
*
* Author: Shravan Gupta
*/

#include "Stack.h"
#include <cstddef>
using namespace std;


Stack::Stack()
{
	head = NULL;
	tail = NULL;
}

Stack::~Stack()
{
	StackNode *p = head;
	while (p != NULL) {
		head = head->next;
		delete p;
		p = head;
	}
	//delete head;
	//delete tail;
}

void Stack::push(int x)
{
	StackNode* newNode = new StackNode();
	newNode->data = x;
	newNode->next = NULL;
	if (isEmpty())
	{
		head = newNode;
		tail = newNode;
	}
	else
	{
		tail->next = newNode;
		tail = newNode;
	}
}//push

int Stack::pop()
{
	if (!isEmpty())
	{
		int data = tail->data;
		StackNode* iter = head;

		if (iter->next == NULL)
		{
			tail = NULL;;
			head = NULL;
			delete iter->next;
		}

		else if (iter->next->next == NULL)
		{
			tail = iter;
			delete iter->next->next;
			iter->next = NULL;
		}

		else
		{
			while (iter->next->next != NULL)
			{
				iter = iter->next;
			}
			tail = iter;
			delete iter->next->next;
			iter->next = NULL;
		}
		return data;
	}
}//pop

int Stack::peek() const
{
	if (!isEmpty())
	{
		return tail->data;
	}
}//peek

bool Stack::isEmpty() const
{
	if (head == NULL && tail == NULL)
		return true;
	else
		return false;
}//isEmpty